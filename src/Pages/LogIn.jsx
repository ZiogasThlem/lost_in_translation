import React from "react";
import LoginForm from "../components/LoginComp/LoginForm";
import LoginHeader from "../components/LoginComp/LoginHeader";

const LogIn = () => {
  return (
    <>
      <LoginHeader />
      <LoginForm />
    </>
  );
};

export default LogIn;
